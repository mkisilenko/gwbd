var assert = require('chai').assert;
var _ = require('lodash');
var moment = require('node-moment');

var Task = require('./Task');

// Modules we use:
// =======================================

// Function returns promise resolved with the cart data from database. Usage:
// get_cart(shop_url, cart_id) 
// You need to provide the `shop_url` that is the handle for the shop own that cart, and the `cart_id` that is a handle for the desired cart
// The data from the database will resolve as an object (database columns become the object properties)
// var get_cart = require('../models/cart');

// Mysql connections, this is a pool of connections that already created and connected with full access to our selected correct database.
// This is a pool so you don't need to db_pool.end() to close connection (you can't):
// this module is using https://www.npmjs.com/package/promise-mysql module, to wrap mysql with Promises results.
// var db_pool = require('../../db/db-pool');

class BirthdayReminderTask extends Task {

    /**
     * This function takes only the needed parameters from the parameters registered for that task
     * This function returns object which will be registered as the class: `this.task_data` object
     * It runs: 1 - When the task is registered, 2 - When the task is fired
     * @param {Object} task_params_obj The object contains all the parameters registered for the task
     
     **************************************************
     [MichaelK] not sure this is what you have meant, 
     I have used it to pull a dummy object only for my POC of the task
     In real-life I would implement a paginated\recursive fetch over db (or even make a daily aggregation for a table set for all birthday cards-today)
     **************************************************

     */
    make_task_data (task_params_obj) {
        return {
            cart_id : '',
            created : 1295850281814,
            card_message : 'sdadssd birthday asdasd',
            shop_url : 'http://awesomeStore'
        }
    }
    
    /**
     * isBirthdayExistsInMsg function will check whether "birthday" string exists in card's message
     * @param {String} msg
     * return {Boolean}
    */
    isBirthdayExistsInMsg (msg){
        var _test = msg.match(/birthday/ig);
        return _test && _test.length > 0 ? true : false;
    }

    /**
     * isABirthday function will check whether sent timestamp is a "birthday"
     * @param {Int} ts
     * return {Boolean}
    */
    isABirthday (ts){
        var _daysSincePurchase = moment().diff(ts, 'days');
        return _daysSincePurchase >= 365 && _daysSincePurchase % 365 == 0;
    }

    /**
     * testCartObj function will check whether or not cart is a "birthday" typed 
     * and it is a birthday since its creation (days since creation % 365 will make the magic)
     * @param {Object} cartObj
     * return {Boolean}
    */
    testCartObj (cartObj){
        return !_.isEmpty(cartObj.card_message) 
                && this.isBirthdayExistsInMsg(cartObj.card_message)
                && this.isABirthday(cartObj.created);
    }

    /**
     * This must be implemented function is the place to assert existence and run validations for the Task's REQUIRED parameters
     * It runs: 1 - When the task is registered, 2 - When the task is fired
     * If you keep it empty it's fine, and then all of the parameters are "Optional"
     * @param {Object} task_params_obj 
     */
    validate (task_params_obj) { }

    

    /**
     * This function will be run when the time for the task arrives and the task is fired.
     * Run here the real tasks of the Task. The Promise must be resolved with truthy value, preferably a message of task success
     */
   async fire_task () {

        // What the task do...
        var _result = await this.testCartObj(this.make_task_data); 
        
        return {sendMail : _result};
    }
}

module.exports = BirthdayReminderTask;

/* [MichaelK] - you may uncomment this just to test it 
var p = new BirthdayReminderTask();

p.fire_task().then(x=>console.log(x));
*/

