//Task init

class Task {

    /**
     * This function takes only the needed parameters from the parameters registered for that task
     * This function returns object which will be registered as the class: `this.task_data` object
     * It runs: 1 - When the task is registered, 2 - When the task is fired
     * @param {Object} task_params_obj The object contains all the parameters registered for the task
     */
    make_task_data (task_params_obj) {
        return {}
    }

}

module.exports = Task;